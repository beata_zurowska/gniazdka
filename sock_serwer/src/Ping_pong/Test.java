package Ping_pong;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


public class Test extends JFrame implements KeyListener, Runnable{
	

	private static final long serialVersionUID = 1L;
	private static final String TITLE  = "ping-pong";	
	private static final int    WIDTH  = 800;		 
	private static final int    HEIGHT = 460;		
	private String servername = "servername" , clientname = "clientname";
 
	
	public Test(){

		setLayout(null);
		JLabel tekst=new JLabel("Naci�nij S aby utowrzy� konto serwera, C aby utworzy� konto klienta");
		tekst.setBounds(150,50,3000,300);
	    add(tekst);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		setVisible(true);
		setTitle(TITLE);
		setSize(WIDTH,HEIGHT);
		addKeyListener(this);
	} 
	
	public static void main(String[] args){
		Test newT = new Test();
		newT.run();

	}
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
		int    keyCode = arg0.getKeyCode();
		String portAdd = null;
		String ipAdd   = null;
		
		if(keyCode==KeyEvent.VK_S){
			
			portAdd = JOptionPane.showInputDialog(null, "Port", "Wprowadz port: ", 1);
			
			if(portAdd!=null){
				if(!isPort(portAdd)){
					JOptionPane.showMessageDialog(null, "Dobry port", "Error!", 1);
				}
			
			else{
				
				servername = JOptionPane.showInputDialog(null, "Nick:", "Wprowadz nick gracza(serwer)", 1);
				servername+="";
				
				if(servername.length()>10 || servername.length()<3 || servername.startsWith("null")){
					JOptionPane.showMessageDialog(null, "Prawidlowy nick", "Error!", 1);
					
				} 
				
				else{
					
					PongServer myServer = new PongServer(servername,portAdd);
					Thread myServerT = new Thread(myServer);
					myServerT.start();
					this.setVisible(false);
				}
				}
			}
		}
		
		if(keyCode==KeyEvent.VK_C){
			
			ipAdd = JOptionPane.showInputDialog(null, "IP", "Wprowadz IP:", 1);
			
			if(ipAdd!=null){
				
				if(!isIPAddress(ipAdd)){
					JOptionPane.showMessageDialog(null, "Prawidlowe IP", "Wprowadz IP", 1);
				}
				
				else{
					portAdd = JOptionPane.showInputDialog(null, "Port", "Wprowadz Port:", 1);
					
					if(portAdd!=null){
						if(!isPort(portAdd)){
							JOptionPane.showMessageDialog(null, "Prawidlowy Port", "Error!:", 1);
						}
						else{
							clientname = JOptionPane.showInputDialog(null, "Nick:", "Wprowadz nick gracza(client):", 1);
							clientname += "";
							if(clientname.length()>10 || clientname.length()<3 || clientname.startsWith("null")){
								JOptionPane.showMessageDialog(null, "Prawidlowy nick", "Error!", 1);
							}
							else{
								PongClient myClient = new PongClient(clientname, portAdd, ipAdd);
								Thread myClientT = new Thread(myClient);
								myClientT.start();
								this.setVisible(false);
							}	
						}
					}
				}
			}
		}
}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	//czy jest prawidlowy port //
	private boolean isPort(String str) {  
		  Pattern pPattern = Pattern.compile("\\d{1,4}");  
		  return pPattern.matcher(str).matches();  
		}  
	 // czy jest prawidlowe ip //
	private boolean isIPAddress(String str) {  
		  Pattern ipPattern = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");  
		  return ipPattern.matcher(str).matches();  
		}

 
}